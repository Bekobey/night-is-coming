﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevelCollider : MonoBehaviour
{
    public SceneLoader SL;
    public string nextLevel;
    public GameObject LoadingScene;
    private void OnCollisionEnter2D(Collision2D col)
    {

        if (col.gameObject.tag == "Player")
        {
            SL.LoadLevelAsync(nextLevel);
            LoadingScene.SetActive(true);
        }
    }
}
