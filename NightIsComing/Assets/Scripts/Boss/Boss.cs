﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : MonoBehaviour {

    public int health;
    public int damage;
    public float attackSpeed = .2f;
    public float hitDistance = 5;
    public float distanceToJump = 15;
    public float speed = 5;
    public float jumpAnimationDuration;
    public float jumpAttackRadius;
    float attackCooldown;
    bool isJumping;
    bool jumped;

    GameObject player;
    Player playerScript;
    Animator anim;
    public GameManager gm;
    public Slider healthBar;

    public GameObject EnemySlash;
    public GameObject bossLandingExplosion;
    public Transform SlashParticleSpawnPosition;

    public bool isDead;
    float direction;
    float distance;

    void Start()
    {
        player = GameObject.FindWithTag("Player");
        playerScript = player.GetComponent<Player>();
        anim = GetComponent<Animator>();

        direction = 1;

        gm.am.Play("BossIntro");
        gm.am.Play("BossBackground");
    }

    private void Update()
    {
        if (attackCooldown > 0) 
        {
            attackCooldown -= Time.deltaTime;
        }

        distance = Vector2.Distance(transform.position, player.transform.position);

        if (distance <= distanceToJump)
        {
            if (!isJumping)
            {
                if (distance <= hitDistance && attackCooldown <= 0)
                {
                    if (!jumped)
                    {
                        StartCoroutine(HitPlayer());
                    }
                }

                else if (!jumped)
                {
                    if (distance > hitDistance)
                    {
                        anim.SetBool("Walk", true);
                        anim.SetBool("Idle", false);
                        transform.position = Vector2.MoveTowards(transform.position, new Vector2(player.transform.position.x, transform.position.y), speed * Time.deltaTime);
                    }

                    else
                    {
                        anim.SetBool("Idle", true);
                        anim.SetBool("Walk", false);
                    }
                }
            }
        }

        else if (!isJumping)
        {
            isJumping = true;
            StartCoroutine(JumpAttack());
        }

        if (isJumping)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(player.transform.position.x, transform.position.y), speed * 3 * Time.deltaTime);
        }

        if (player.transform.position.x > transform.position.x && direction > 0)
        {
            Flip();
        }

        else if (player.transform.position.x < transform.position.x && direction < 0)
        {
            Flip();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player Weapon")
        {
            gm.am.Play("Hit");
            health -= playerScript.damage;
            healthBar.value = health;
            GameObject newParticle = Instantiate(EnemySlash, SlashParticleSpawnPosition.position, Quaternion.identity);
            Destroy(newParticle, 1);

            if (health <= 0 && !isDead)
            {
                Die();
            }
        }
    }

    void Flip()
    {
        direction = -direction;

        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, 1);
    }

    void Die()
    {
        StartCoroutine(Dying());
    }

    private IEnumerator Dying()
    {
        gm.am.Play("BossDeath");
        anim.SetTrigger("Die");
        isDead = true;
        yield return new WaitForSecondsRealtime(1.5f);
        gameObject.SetActive(false);
    }

    private IEnumerator JumpAttack()
    {
        anim.SetTrigger("Jump");
        isJumping = true;
        yield return new WaitForSeconds(.3f);
        GameObject newParticle1 = Instantiate(bossLandingExplosion, transform.position, Quaternion.identity);
        Destroy(newParticle1, 1);
        jumped = true;
        yield return new WaitForSeconds(jumpAnimationDuration);
        GameObject newParticle2 = Instantiate(bossLandingExplosion, transform.position, Quaternion.identity);
        Destroy(newParticle2, 1);
        isJumping = false;
        jumped = false;

    }

    IEnumerator HitPlayer()
    {
        anim.SetTrigger("Attack 1");
        attackCooldown = 1 / attackSpeed;

        yield return new WaitForSeconds(.3f);
        if(!playerScript.isJumping)
        playerScript.UpdateHealth(damage);
    }
}
