﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class introBehavior : StateMachineBehaviour {

    private int randomAnimation;

	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
    {
        randomAnimation = Random.Range(0, 2);

        if (randomAnimation == 0)
        {
            animator.SetTrigger("Idle");
        }

        else {

            animator.SetTrigger("Jump");
        }
	}
}
