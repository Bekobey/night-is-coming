﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemy : MonoBehaviour
{
    [Header("Combat Stats")]
    public int minDamage;
    public int maxDamage;

    public GameObject FlyingEnemyExplosion;
    public GameObject FlyingEnemyDie;

    GameObject player;
    private GameManager gm;
    Player playerScript;
    void Start()
    {
        player = GameObject.FindWithTag("Player");
        playerScript = player.GetComponent<Player>();
        gm = GetComponentInParent<GameManager>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            HitPlayer();
        }
        if (col.tag == "Player Weapon")
        {
                Die();
        }
    }
    void HitPlayer()
    {
        if (playerScript.isBlocking)
            return;


        int damage = Random.Range(minDamage, maxDamage);
        playerScript.UpdateHealth(damage);
        Explode();
    }

    void Die()
    {
        gm.am.Play("Hit");
        gameObject.SetActive(false);
        GameObject newParticle = Instantiate(FlyingEnemyDie, transform.position, Quaternion.identity);
        //newParticle.transform.parent = particleParentObject.transform;
        Destroy(newParticle, 1);
    }
    void Explode()
    {
        gm.am.Play("PlayerHit");
        gameObject.SetActive(false);
        GameObject newParticle = Instantiate(FlyingEnemyExplosion, transform.position, Quaternion.identity);
        //newParticle.transform.parent = particleParentObject.transform;
        Destroy(newParticle, 1);
    }
}
